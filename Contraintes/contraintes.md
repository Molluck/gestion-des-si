-Contrainte de temps: le temps imparti pour la réalisation de notre projet est relativement court.

-Contrainte technique: l'analyse génomique requiert généralement des supercalculateurs. Néanmoins, dans le cadre de notre projet-pilote, cela ne représentera pas de problème.

-Contrainte de connaissance: La bibliothèque BioPython demande à être étudiée soigneusement avant toute ébauche.

-Contrainte de personnel: Une seule personne est à la charge du projet.
