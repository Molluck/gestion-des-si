#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Tkinter import *
from tkMessageBox import *
import Tkinter, Tkconstants, tkFileDialog

def sequence():
    #FilePath
    filename = tkFileDialog.askopenfilename(title= "Ouvrir séquence", filetypes=[('geneBank file', '.gb'), ('fasta file', '.fasta'), ('tout les fichiers', '.*')])
    fichier = open(filename, "r")
    content = fichier.read()
    fichier.close()

    txt = canvas1.create_text(100, 100, text= content, font= "Arial 16 italic", fill= "blue")



def callback():
    if(askyesno("Exemple1", "Aimez-vous BioPythonApp?")):
            showwarning("Exemple2", "Bon choix!")

    if(askquestion("Combien font 7*9?")):
        askokcancel("fermer?", "Fermer?")
    else:
            showinfo("info", "Dommage...")
            showerror("erreur", "Erreur")

def clicked():
    res = "Bienvenu" + txt1.get()
    label1.configure(text= res)

def radioBoutton():
    canvas1.delete('all')
    txt = canvas1.create_text(100, 100, text= valeurs.get(int(v.get()), "invalide"), font= "Arial 16 italic", fill= "blue")

def recupere():
    showinfo("Donnée reçu", valeurEntree1.get())

    


window = Tk()
window.title("BioPython")
window.geometry("1000x1000")

Frame1 = Frame(window, borderwidth= 2, relief= GROOVE)
Frame1.grid(row= 0, column= 0)
Frame2 = Frame(window, borderwidth= 2, relief= GROOVE)
Frame2.grid(row= 0, column= 1)



#Menu
menuBar = Menu(Frame1)

menu1 = Menu(menuBar, tearoff= 0)
menu1.add_command(label= "Ouvrir Séquence", command= sequence)
menu1.add_command(label= "Editer", command= sequence)
menu1.add_separator()
menu1.add_command(label= "Quitter", command= window.quit)
menuBar.add_cascade(label= "Fichier", menu= menu1)

window.config(menu= menuBar)

label1 = Label(Frame1, text="Hello", font=("Arial Bold", 20))
label1.grid(column=0, row=0)
txt1 = Entry(Frame1, width=10)
txt1.grid(column=0, row=1)
bouton1 = Button(Frame1, text="test", command= clicked)

#Canvas
v = StringVar(Frame1, '0')
canvas1 = Canvas(Frame2, width= 400, height= 400, background= 'white')
canvas1.grid(column= 1, row= 4)


#RadioBoutton
valeurs = {
        1 : "nucléotide",
        2 : "gène",
        3 : "protéine"
}
i=3
j=0
for (valeur, texte) in valeurs.items() :
    Radiobutton(Frame1, text= texte, variable= v, value= valeur, indicator= 0,
        background= 'light blue', command= radioBoutton).grid(row= i, ipady= 5)
    i += 1
    j += 1


#CheckBox
chk_state = BooleanVar()
chk_state.set(True)
chk = Checkbutton(Frame1, text='Choose', var= chk_state)
chk.grid(column=0, row=7)

#Liste
liste1 = Listbox(Frame1)
liste1.insert(1, "ADN")
liste1.insert(2, "ARN")
liste1.insert(3, "CODON")
liste1.grid(row=8)

#Scale
valeur = DoubleVar()
scale = Scale(Frame2, variable = valeur)
scale.grid(column= 3)

#lertes
Button(Frame1, text="Action1", command= callback).grid(row= 9)
Button(Frame2, text ="spider", relief=RAISED, cursor="spider").grid(row= 10)

valeurEntree1 = StringVar()
valeurEntree1.set("Valeur")
entree1 = Entry(Frame2, textvariable= valeurEntree1, width= 30)
entree1.grid(row= 5)

btnEntree1 = Button(Frame2, text= "Valider", command= recupere).grid(row= 6)
window.mainloop()
