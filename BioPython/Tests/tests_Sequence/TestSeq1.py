#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from Bio.Alphabet import generic_nucleotide
from Bio.Alphabet import IUPAC

print("Fichier test1: Etude de l'objet Python Seq permettant la manipulation de séquences nucléotidique\n")

seq1 = Seq("AGCCGATCGTAAGCTAACCTG", IUPAC.unambiguous_dna)
print("Séquence1: %s \nTaille: %i \nNombre de A: %i \nNombre de C: %i \nNombre de G: %i \nNombre de T: %i" % (str(seq1), len(seq1), seq1.count("A"), seq1.count("C"), seq1.count("G"), seq1.count("T")))

print("\nInverse de la séquence: %s" % seq1[::-1])

print("Concaténation d'une séquence nucléotidique générique et d'une séquence ADN\n")
nuc_seq = Seq("AGCCTAATCG", generic_nucleotide)
dna_seq = Seq("AGGA", IUPAC.unambiguous_dna)
print("Séquence nucléotide: %s" % nuc_seq)
print("Séquence ADN: %s" % dna_seq)
print("Concaténation: %s" % nuc_seq + dna_seq)
nuc_seq + dna_seq

liste_seqs = [Seq("AGCT", generic_dna), Seq("ACCC", generic_dna), Seq("TAAT", generic_dna)]
print("\nListe de séquences: %s\n" % liste_seqs)
seq2 =  sum(liste_seqs, Seq("", generic_dna))
print("Concaténation: %s" % seq2)
print("Complément nucléotidique: %s" % seq2.complement())
print("-Note: A-T et G-C")
print("Inverse du complément: %s" % seq2.reverse_complement())
print("-Note:L'inverse du complément permet d'obtenir l'ARN messager (ARNm) après transcription.")
print("Transciption(ARNm): %s" % seq2.reverse_complement().transcribe())

